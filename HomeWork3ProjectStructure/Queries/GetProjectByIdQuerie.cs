﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Models;
using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetProjectByIdQuerie : IQuerie<ProjectDTO>
    {
        public int Id { get; set; }
    }
}
