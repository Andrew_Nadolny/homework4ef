﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeWork3ProjectStructure.Interfaces;
using Common.Models;
using Common.DTO;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetUserTasksFinishedInCurrentYearQuerie : IQuerie<List<TaskDTO>>
    {
        public int Id { get; set; }
    }
}
