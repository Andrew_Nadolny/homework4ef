﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Models;
using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;


namespace HomeWork3ProjectStructure.Queries
{
    public class GetTaskByIdQuerie : IQuerie<TaskDTO>
    {
        public int Id { get; set; }

    }
}
