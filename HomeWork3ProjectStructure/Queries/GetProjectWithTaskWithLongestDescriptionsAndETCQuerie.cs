﻿using Common.Models;
using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetProjectWithTaskWithLongestDescriptionsAndETCQuerie : IQuerie<List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)>>
    {
    }
}
