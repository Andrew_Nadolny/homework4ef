﻿using Common.Models;
using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork3ProjectStructure.Commands
{
    public class CreateTaskCommand : ICommand<TaskDTO>
    {
        public TaskDTO task { get; set; }
    }
}
