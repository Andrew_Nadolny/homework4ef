﻿using Common.Models;
using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class CreateTeamCommand : ICommand<TeamDTO>
    {
        public TeamDTO team { get; set; }
    }
}
