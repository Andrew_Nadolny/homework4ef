﻿using Common.Models;
using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork3ProjectStructure.Commands
{
    public class DeleteTaskByItemCommand : ICommand<bool>
    {
        public TaskDTO task { get; set; }
    }
}
