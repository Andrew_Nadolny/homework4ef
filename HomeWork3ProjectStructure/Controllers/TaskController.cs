﻿using AutoMapper;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.UnitOfWork;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.DAL;
using Common.DTO;

// For more information on enabling Web API for empty tasks, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3TaskStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly QuerieProcessor _querieProcessor;
        private readonly CommandProcessor _commandProcessor;

        public TaskController(ProjectsDbContext context, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(context, mapper));
            _commandProcessor = new CommandProcessor(new CommandHandler(context, mapper));

        }
        // GET: api/<TaskController>
        [HttpGet]
        public string Get()
        {
            return JsonConvert.SerializeObject( _querieProcessor.Processed(new GetTasksQuerie()));
        }

        // GET api/<TaskController>/5
        [HttpGet("{id}")]
        public string GetAsync(int id)
        {
            return JsonConvert.SerializeObject( _querieProcessor.Processed(new GetTaskByIdQuerie() { Id = id }));
        }

        // POST api/<TaskController>
        [HttpPost]
        public async System.Threading.Tasks.Task<string> PostAsync([FromBody] TaskDTO task)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new CreateTaskCommand() { task = task }));
        }

        // PUT api/<TaskController>/5
        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task<string> Put([FromBody] TaskDTO task)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new UpdateTaskCommand() { task = task }));

        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<bool> DeleteAsync(int id)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTaskByIdCommand() { Id = id });
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<bool> Delete([FromBody] TaskDTO task)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTaskByItemCommand() { task = task });

        }

    }
}
