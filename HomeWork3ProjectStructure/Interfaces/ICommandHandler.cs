﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Interfaces
{
    public interface ICommandHandler<TCommand, TResult> where TCommand : ICommand<TResult>
    { 
        public Task<TResult> HandlerAsync(TCommand command);
        //public void Handler(TCommand command);

    }
}
