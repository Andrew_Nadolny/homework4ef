﻿using AutoMapper;
using Common.Models;
using HomeWork3ProjectStructure.DAL;
using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using HomeWork3ProjectStructure.Queries;
using HomeWork3ProjectStructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Handlers
{
    public class QuerieHandler : IQuerieHandler<GetProjectsQuerie, List<ProjectDTO>>,
        IQuerieHandler<GetTasksQuerie, List<TaskDTO>>,
        IQuerieHandler<GetTeamsQuerie, List<TeamDTO>>,
        IQuerieHandler<GetUsersQuerie, List<UserDTO>>,
        IQuerieHandler<GetProjectByIdQuerie, ProjectDTO>,
        IQuerieHandler<GetTaskByIdQuerie, TaskDTO>,
        IQuerieHandler<GetTeamByIdQuerie, TeamDTO>,
        IQuerieHandler<GetCountTasksInProjectByAuthorIdQuerie, Dictionary<int, int>>,
        IQuerieHandler<GetListOfTeamsWithTeamatesOlderThen10Querie, List<(int Id, string Name, List<UserDTO> Teammates)>>,
        IQuerieHandler<GetListOfUsersAscendingWhitTasksDescendingQuerie, List<(UserDTO User, List<TaskDTO> Tasks)>>,
        IQuerieHandler<GetProjectWithTaskWithLongestDescriptionsAndETCQuerie, List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)>>,
        IQuerieHandler<GetUserTasksFinishedInCurrentYearQuerie, List<TaskDTO>>,
        IQuerieHandler<GetUserTasksWithShortNameByUserIdQuerie, List<TaskDTO>>,
        IQuerieHandler<GetUserWithLastProjectAndETCQuerie, List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)>>
    {
        private readonly UnitOfWork.UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public QuerieHandler(ProjectsDbContext context,IMapper mapper)
        {
            _unitOfWork = new UnitOfWork.UnitOfWork(context);
            _mapper = mapper;
        }
        public List<Project> Handle(GetProjectsStructureQuerie querie)
        {
            List<Project> projects = _unitOfWork.Set<Project>().Get();
            List<Team> teams = _unitOfWork.Set<Team>().Get();
            List<Common.Models.Task> tasks = _unitOfWork.Set<Common.Models.Task>().Get();
            List<User> users = _unitOfWork.Set<User>().Get();
            if (projects != null && teams != null && tasks != null && users != null)
            {
                return projects.Select
                    ( p => new Project { Id = p.Id, AuthorId = p.AuthorId, CreatedAt = p.CreatedAt, Deadline = p.Deadline, Description = p.Description, Name = p.Name, TeamId = p.TeamId, Tasks = tasks.Where(x => x.ProjectId == p.Id).ToList() }).ToList();
            }
            return null;

        }
        public List<ProjectDTO> Handle(GetProjectsQuerie querie)
        {
            return _mapper.Map<List<ProjectDTO>>( _unitOfWork.Set<Project>().Get());
        }
        public  List<TaskDTO> Handle(GetTasksQuerie querie)
        {
            return _mapper.Map<List<TaskDTO>>( _unitOfWork.Set<Common.Models.Task>().Get());
        }
        public List<TeamDTO> Handle(GetTeamsQuerie querie)
        {
            return _mapper.Map<List<TeamDTO>>(_unitOfWork.Set<Team>().Get());
        }
        public List<UserDTO> Handle(GetUsersQuerie querie)
        {
            return _mapper.Map<List<UserDTO>>(_unitOfWork.Set<User>().Get());
        }
        public  ProjectDTO Handle(GetProjectByIdQuerie querie)
        {
            return _mapper.Map<ProjectDTO>( _unitOfWork.Set<Project>().Get(querie.Id));
        }
        public  TaskDTO Handle(GetTaskByIdQuerie querie)
        {
            return _mapper.Map<TaskDTO>( _unitOfWork.Set<Common.Models.Task>().Get(querie.Id));
        }
        public  TeamDTO Handle(GetTeamByIdQuerie querie)
        {
            return _mapper.Map<TeamDTO>( _unitOfWork.Set<Team>().Get(querie.Id));
        }
        public UserDTO Handle(GetUserByIdQuerie querie)
        {
            return _mapper.Map<UserDTO>(_unitOfWork.Set<User>().Get(querie.Id));
        }
        public Dictionary<int, int> Handle(GetCountTasksInProjectByAuthorIdQuerie querie)
        {
            var tasks = _unitOfWork.Set<Common.Models.Task>().Get();
            return _unitOfWork.Set<Project>().Get().Where(x => x.AuthorId == querie.Id).ToDictionary(x => x.Id, x => tasks.Select(task => task.ProjectId == x.Id && task.PerformerId == querie.Id ).Count());
        }
        public List<(int Id, string Name, List<UserDTO> Teammates)> Handle(GetListOfTeamsWithTeamatesOlderThen10Querie querie)
        {
            var projects = Handle(new GetProjectsStructureQuerie());

            return projects.Select(x =>
                 (Id: x.TeamId,
                 Name: _unitOfWork.Set<Team>().Get(x.TeamId)?.Name,
                 Teammates: _mapper.Map<List<UserDTO>>(_unitOfWork.Set<User>().Get()?.Where(x => x.TeamId == x.TeamId).OrderBy(x => x.RegisteredAt))
                 )).Where(x => !x.Teammates.Any(x => x.BirthDay > DateTime.Now.AddYears(-10))).ToList(); ;
        }
        public List<(UserDTO User, List<TaskDTO> Tasks)> Handle(GetListOfUsersAscendingWhitTasksDescendingQuerie querie)
        {
            var users = _unitOfWork.Set<User>().Get();
            var tasks = _unitOfWork.Set<Common.Models.Task>().Get();

            return users.Distinct().OrderBy(x => x.Name.Length)
                .Select(
                user => (
                    User: _mapper.Map<UserDTO>(user),
                    Tasks: _mapper.Map<List<TaskDTO>>(tasks.Where(x => x.PerformerId == user.Id).OrderByDescending(x => x.Name.Length).ToList())
            )).ToList();
        }
        public List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)>
            Handle(GetProjectWithTaskWithLongestDescriptionsAndETCQuerie querie)
        {
            var projects = Handle(new GetProjectsStructureQuerie());
            var user = _unitOfWork.Set<Project>().Get();
            return projects.Select(project => (
            Project: _mapper.Map<ProjectDTO>(project),
            TaskWithLongestDescriptions: _mapper.Map<TaskDTO>(project.Tasks.DefaultIfEmpty().Aggregate((longest, next) =>
                        next.Description.Length > longest.Description.Length ? next : longest)),
            TaskWithShortestName: _mapper.Map <TaskDTO>(project.Tasks.DefaultIfEmpty().Aggregate((shortests, next) =>
                        next.Name.Length < shortests.Name.Length ? next : shortests)),
            CountOfUsersInProjects: (project.Description.Length > 20 || project.Tasks.Count() < 3) ? user.Where(x => x.TeamId == project.TeamId).Count() : 0
            )).ToList();
        }
        public List<TaskDTO> Handle(GetUserTasksFinishedInCurrentYearQuerie querie)
        {
            return (Handle(new GetTasksQuerie())).Where(x => x.FinishedAt.Value.Year == DateTime.Now.Year && x.PerformerId == querie.Id).ToList();
        }
        public List<TaskDTO> Handle(GetUserTasksWithShortNameByUserIdQuerie querie)
        {
            return Handle(new GetTasksQuerie()).Where(x => x.PerformerId == querie.Id && x.Name.Length < 45).ToList();
        }
        public List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)> Handle(GetUserWithLastProjectAndETCQuerie querie)
        {
            var projects = Handle(new GetProjectsStructureQuerie());
            var teams = _unitOfWork.Set<Team>().Get();
            var tasks = _unitOfWork.Set<Common.Models.Task>().Get();
            var users = _unitOfWork.Set<User>().Get();


            return users.Select(user => (
               User: _mapper.Map<UserDTO>(user),
               LastProject: _mapper.Map<ProjectDTO>(projects.Where(project => project.AuthorId == user.Id).OrderBy(project => project.CreatedAt)?.FirstOrDefault()),
               LastProjectTask: _mapper.Map<List<TaskDTO>>(projects.Where(project => project.AuthorId == user.Id).OrderBy(project => project.CreatedAt).FirstOrDefault()?.Tasks),
               CountOfUnfinishedAndCanceledTasks: tasks.Where(x => x.PerformerId == user.Id && (x.FinishedAt != DateTime.MinValue || x.State == TaskState.Canceled)).Count(),
               LongesUserTask: _mapper.Map<TaskDTO>(tasks.Where(task => task.PerformerId == user.Id).DefaultIfEmpty().Aggregate((longest, next) =>
                      (next.FinishedAt - next.CreatedAt) > (longest.FinishedAt - longest.CreatedAt) ? next : longest))
           )).ToList();
        }
    }
}
