﻿using AutoMapper;
using Common.Models;
using HomeWork3ProjectStructure.Commands;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.UnitOfWork;
using HomeWork3ProjectStructure.Interfaces;
using Common.DTO;
using HomeWork3ProjectStructure.DAL;

namespace HomeWork3ProjectStructure.Handlers
{
    public class CommandHandler : ICommandHandler<CreateProjectCommand, ProjectDTO>,
          ICommandHandler<CreateTaskCommand, TaskDTO>,
          ICommandHandler<CreateTeamCommand, TeamDTO>,
          ICommandHandler<CreateUserCommand, UserDTO>,
          ICommandHandler<UpdateProjectCommand, ProjectDTO>,
          ICommandHandler<UpdateTaskCommand, TaskDTO>,
          ICommandHandler<UpdateTeamCommand, TeamDTO>,
          ICommandHandler<UpdateUserCommand, UserDTO>,
          ICommandHandler<DeleteProjectByIdCommand, bool>,
          ICommandHandler<DeleteTaskByIdCommand, bool>,
          ICommandHandler<DeleteTeamByIdCommand, bool>,
          ICommandHandler<DeleteUserByIdCommand, bool>,
          ICommandHandler<DeleteProjectByItemCommand, bool>,
          ICommandHandler<DeleteTaskByItemCommand, bool>,
          ICommandHandler<DeleteTeamtByItemCommand, bool>,
          ICommandHandler<DeleteUserByItemCommand, bool>

    {
        private readonly HomeWork3ProjectStructure.UnitOfWork.UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommandHandler(ProjectsDbContext context, IMapper mapper)
        {
            _unitOfWork = new UnitOfWork.UnitOfWork(context);
            _mapper = mapper;
        }

        public async Task<ProjectDTO> HandlerAsync(CreateProjectCommand command)
        {
            var project = await _unitOfWork.Set<Project>().CreateAsync(_mapper.Map<Project>(command.project));
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<TaskDTO> HandlerAsync(CreateTaskCommand command)
        {
            var task = await _unitOfWork.Set<Common.Models.Task>().CreateAsync(_mapper.Map<Common.Models.Task>(command.task));
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TeamDTO> HandlerAsync(CreateTeamCommand command)
        {
            var team = await _unitOfWork.Set<Team>().CreateAsync(_mapper.Map<Team>(command.team));
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<UserDTO> HandlerAsync(CreateUserCommand command)
        {
            var user =  _mapper.Map<UserDTO>(_unitOfWork.Set<User>().CreateAsync(_mapper.Map<User>(command.user)));
            await _unitOfWork.SaveChangesAsync();
            return user;
        }

        public async Task<ProjectDTO> HandlerAsync(UpdateProjectCommand command)
        {
            var project = _mapper.Map<ProjectDTO>(_unitOfWork.Set<Project>().Update(_mapper.Map<Project>(command.project)));
            await _unitOfWork.SaveChangesAsync();
            return project;
        }

        public async Task<TaskDTO> HandlerAsync(UpdateTaskCommand command)
        {
            var task = _mapper.Map<TaskDTO>(_unitOfWork.Set<Common.Models.Task>().Update(_mapper.Map<Common.Models.Task>(command.task)));
            await _unitOfWork.SaveChangesAsync();
            return task;
        }

        public async Task<TeamDTO> HandlerAsync(UpdateTeamCommand command)
        {
            var team = _mapper.Map<TeamDTO>(_unitOfWork.Set<Team>().Update(_mapper.Map<Team>(command.team)));
            await _unitOfWork.SaveChangesAsync();
            return team;
        }

        public async Task<UserDTO> HandlerAsync(UpdateUserCommand command)
        {
            var user = _mapper.Map<UserDTO>(_unitOfWork.Set<User>().Update(_mapper.Map<User>(command.user)));
            await _unitOfWork.SaveChangesAsync();
            return user;
        }

        public async Task<bool> HandlerAsync(DeleteProjectByIdCommand command)
        {
           var isDelete = _unitOfWork.Set<Project>().Delete(command.Id);
           await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTaskByIdCommand command)
        {
            var isDelete = _unitOfWork.Set<Common.Models.Task>().Delete(command.Id);
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTeamByIdCommand command)
        {
            var isDelete = _unitOfWork.Set<Team>().Delete(command.Id);
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteUserByIdCommand command)
        {
            var isDelete = _unitOfWork.Set<User>().Delete(command.Id);
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteProjectByItemCommand command)
        {
            var isDelete = _unitOfWork.Set<Project>().Delete(_mapper.Map<Project>(command.project));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTaskByItemCommand command)
        {
            var isDelete = _unitOfWork.Set<Common.Models.Task>().Delete(_mapper.Map<Common.Models.Task>(command.task));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTeamtByItemCommand command)
        {
            var isDelete = _unitOfWork.Set<User>().Delete(_mapper.Map<User>(command.team));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteUserByItemCommand command)
        {
            var isDelete = _unitOfWork.Set<User>().Delete(_mapper.Map<User>(command.user));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;

        }


    }
}
